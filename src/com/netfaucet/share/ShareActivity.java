package com.netfaucet.share;

import java.net.InetAddress;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.netfaucet.share.adapter.NodeData;
import com.netfaucet.share.adapter.NodeListAdapter;
import com.netfaucet.share.service.IShareServiceListener;
import com.netfaucet.share.service.ShareService;
import com.netfaucet.share.service.ShareService.ShareServiceBinder;
import com.netfaucet.share.utils.CalculateFileLength;
import com.netfaucet.share.utils.ConnectionDetector;
import com.netfaucet.share.utils.Constants;
import com.netfaucet.share.utils.DataManager;
import com.netfaucet.share.utils.DownloadAsync;
import com.netfaucet.share.utils.FileCache;
import com.netfaucet.share.utils.MergeTask;
import com.netfaucet.share.utils.NodeConnectionInfo;
import com.netfaucet.wifip2p.WifiDirectCallbacks;
import com.netfaucet.wifip2p.WifiDirectManager;
import com.samsung.chord.ChordManager;

public class ShareActivity extends Activity  implements IShareServiceListener, OnClickListener, OnItemClickListener{

	private static final String TAG = "ShareActivity";

	private TextView mMyIpAddressTextView;

	private TextView mMyNodeNameTextView;

	private ShareService mShareService = null;

	private Button mConnectBtn;

	private EditText mTypeUrl;

	private TextView  mPublicChannelTextView;

	private ListView mPublicChannelListView;

	private NodeListAdapter mPublicChannelNodeListAdapter = null;

	private Button mWifiDirectButton;

	private LinearLayout mInputLayout;

	private ImageView creditsButton;

	//private String mChannelType = "";

	private HashMap<String, AlertDialog> mAlertDialogMap = null;

	private String mMyNodeName;

	private static final String DONE = "done";

	private static final String SEND_TYPE = "send";
	private static final String RECEIVE_TYPE = "receive";
	public static final String tempFilePath = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/temp/";
	private FileCache fileCache;

	// flag for Internet connection status
	private Boolean isInternetPresent = false;

	// Connection detector class
	private ConnectionDetector cd;

	//Data Manager

	private DataManager dm;

	private WifiDirectManager mWifiDirectManager;

	private TextView mLogConsole;

	private ArrayList<NodeConnectionInfo> mNodeConnection = null;

	private ArrayList<String> mNodeContainsConnectivity = null;
	
	private ArrayList<String> mNodeNoConnectivity = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		//Start Wifi-Direct

		mWifiDirectManager = new WifiDirectManager(getApplicationContext(), mWifiDirectCallbacks);

		mConnectBtn = (Button)findViewById(R.id.connect);
		mMyNodeNameTextView = (TextView)findViewById(R.id.myNodeName_textView);
		mMyIpAddressTextView = (TextView)findViewById(R.id.myIpAddress_textView);
		mPublicChannelTextView = (TextView)findViewById(R.id.publicChannel_textView);
		mPublicChannelTextView.setTextColor(Color.RED);
		mTypeUrl = (EditText)findViewById(R.id.type_url);
		mInputLayout = (LinearLayout)findViewById(R.id.datatest_input_layout);
		mConnectBtn.setOnClickListener(this);

		mWifiDirectButton = (Button)findViewById(R.id.wifidirect_button);
		mWifiDirectButton.setOnClickListener(this);
		mLogConsole = (TextView) findViewById(R.id.log_console);

		creditsButton = (ImageView)findViewById(R.id.credits);
		creditsButton.setOnClickListener(this);
		//public channel list view
		mPublicChannelListView = (ListView)findViewById(R.id.publicChannel_listView);
		mPublicChannelNodeListAdapter = new NodeListAdapter(getApplicationContext(),
				false);

		if(Constants.INTERNAL_BUILD)
			mTypeUrl.setText(Constants.URL_TO_DOWNLOAD);
		//if helper node (and not main node)
		//mInputLayout.setVisibility(View.GONE);


		mPublicChannelListView.setAdapter(mPublicChannelNodeListAdapter);
		mPublicChannelListView.setOnItemClickListener(this);
		mPublicChannelListView.setEnabled(true); // TODO: mPublicChannelListView.setEnabled(false);

		//		for (int i =0; i<10 ;i++) { //For testing
		//			mPublicChannelNodeListAdapter.addNode("nodeName" + i);
		//		}
		mAlertDialogMap = new HashMap<String, AlertDialog>();
		// creating connection detector class instance
		cd = new ConnectionDetector(getApplicationContext());

		dm = new DataManager(getApplicationContext());

		startService();

		fileCache = new FileCache(getApplicationContext());
		/*do{
		    if(ShareService.ServiceStarted==true)
		        startChord();
		}while(mShareService==null);*/

		mNodeConnection = new ArrayList<NodeConnectionInfo>();


	}

	private final WifiDirectCallbacks mWifiDirectCallbacks = new WifiDirectCallbacks() {
		@Override
		public void connectFailed() {
			appendLog("Connection failed!");
			//Nothing to do
		}

		@Override
		public void connected(final InetAddress address, final boolean isMine) {
			appendLog("WiFi Direct is connected!");
			bindChordService();

		}

		@Override
		public void unavailable() {
			appendLog("WiFi Direct is disabled!");
			getWirelessSettingsDialog("WiFi Direct disabled", "Enable in settings").show();
		}

		@Override
		public void discoverFinished(String deviceAddress) {
			appendLog("Discovering successfully finished. Connecting... : Device Address " + deviceAddress);
			mWifiDirectManager.connect(deviceAddress);
		}

		@Override
		public void gotMyMac() {
			appendLog("Received Wifi Direct MAC: " + mWifiDirectManager.getMyAddress());
			mWifiDirectManager.getMyAddress();
		}

		@Override
		public void initializeChord() {

		}
	};

	/**
	 * Creates a dialog which allows to open wireless settings window (used if any communication service is disabled).
	 * 
	 * @param message
	 *            Message text
	 * @param buttonText
	 *            Button text
	 * @return
	 *         AlertDialog to be shown
	 */
	private AlertDialog getWirelessSettingsDialog(final String message, final String buttonText) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(ShareActivity.this);
		builder.setMessage(message).setCancelable(true)
		.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface arg0, final int arg1) {
				startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
				finish();
			}
		})
		.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(final DialogInterface arg0) {
				finish();
			}
		});
		return builder.create();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void startService() {
		Log.i(TAG, "startService()");
		Intent intent = new Intent("com.netfaucet.share.service.ShareService.SERVICE_START");
		startService(intent);
	}

	private void stopService() {
		Log.i(TAG, "stopService()");
		Intent intent = new Intent("com.netfaucet.share.service.ShareService.SERVICE_STOP");
		stopService(intent);
	}

	public void bindChordService() {
		Log.i(TAG, "bindChordService()");
		if (mShareService == null) {
			Intent intent = new Intent(
					"com.netfaucet.share.service.ShareService.SERVICE_BIND");
			bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		}
	}

	private void unbindChordService() {
		Log.i(TAG, "unbindChordService()");

		if (null != mShareService) {
			unbindService(mConnection);
		}
		mShareService = null;
	}

	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "onServiceConnected()");
			ShareServiceBinder binder = (ShareServiceBinder)service;
			mShareService = binder.getService();
			try {
				mShareService.initialize(ShareActivity.this);
				startChord();
			} catch (Exception e) {
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.i(TAG, "onServiceDisconnected()");
			mShareService = null;
		}
	};


	@Override
	protected void onDestroy() {
		mWifiDirectManager.disconnect();
		super.onDestroy();
		clearAllData();
		unbindChordService();
		stopService();
		Log.v(TAG, "onDestroy");
	}


	/**
	 * Check whether received message contain Internet Connection Status.
	 * @param message
	 * @return
	 */
	private boolean isConnectionMessage( String message) {
		String delimiter = ",";
		String[] temp;
		temp = message.split(delimiter);

		if ("Connection".equals(temp[0])){
			return true;
		} else {
			return false;		
		}

	}

	public void onMessageReceived(String nodeName, String channel, String message) {

		//Check whether the received message contains "Connection" start tag.

		if (isConnectionMessage(message)) {

			mNodeConnection.add(new NodeConnectionInfo(message));

			//testing
			for (int i= 0;i<mNodeConnection.size(); i++) {
				appendLog(mNodeConnection.get(i).getNodeName() + "- " + mNodeConnection.get(i).getConnected());
			}

			return;
		} 

		showToastMessage("Nodename: " + nodeName + " " + "Channel: " + channel +" " + "Message: "+ message);
		String status = null;
		try {
			status = new DownloadAsync(getApplicationContext(), 
					Constants.URL_TO_DOWNLOAD, 
					RECEIVE_TYPE, 
					mPublicChannelNodeListAdapter,
					Integer.parseInt(splitString(message)[1]),
					Integer.parseInt(splitString(message)[2])
					).execute().get();
		} catch (NumberFormatException e) {
		} catch (InterruptedException e) {
		} catch (ExecutionException e) {
		}

		if (status.equals(DONE)) {
			ArrayList<String> checkedList = mPublicChannelNodeListAdapter.getNodeList();
			for (String nodeNameR : checkedList) {
				String exchangeId = mShareService.sendFile(mShareService.getPublicChannel(), tempFilePath + fileCache.getFileName(mTypeUrl.getText().toString()), nodeName);
				if (null == exchangeId || exchangeId.isEmpty()) {
					showToastMessage("Sending file failed");
				} else {
					onFileProgress(true, nodeNameR, mShareService.getPublicChannel(), 0, exchangeId);
				}
			}
		}
	}

	@Override
	public void onReceiveMessage(String node, String channel, String message) {
		onMessageReceived(node, channel, message);
	}

	@Override
	public void onFileWillReceive(String node, String channel, String fileName,
			String exchangeId) {
		if (mShareService.acceptFile(channel, exchangeId)) {
			onFileProgress(false, node, channel, 0, exchangeId);
		} 
		//onFileNotify(node, channel, fileName, exchangeId);
	}

	@Override
	public void onFileProgress(boolean bSend, String nodeName, String channel,
			int progress, String exchangeId) {
		String message = bSend ? getString(R.string.sending_to_ps, nodeName) : getString(
				R.string.receiving_from_ps, nodeName);
		showToastMessage(message);
	}


	private void displayFileNotify(final String nodeName, final String channel,
			final String fileName, final String transactionId) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle(channel)
				.setMessage(getString(R.string.from_ps_file_ps, nodeName, fileName))
				.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						if (mShareService.acceptFile(channel, transactionId)) {
							onFileProgress(false, nodeName, channel, 0, transactionId);
						} 
					}
				}).setNegativeButton(R.string.reject, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						if (!mShareService.rejectFile(channel, transactionId))
							Log.e(TAG,  "displayFileNotify : fail to rejectFile");

					}
				}).create();
		alertDialog.show();

		alertDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface arg0) {
				if (!mShareService.rejectFile(channel, transactionId))
					Log.e(TAG, "displayFileNotify : fail to rejectFile");
			}
		});

		mAlertDialogMap.put(transactionId, alertDialog);
	}


	@Override
	public void onFileCompleted(int reason, String nodeName, String channel,
			String exchangeId, String fileName) {
		Log.d(TAG, "onFileCompleted : " + exchangeId);
		String msg = null;
		boolean bMine = true;

		switch (reason) {
		case IShareServiceListener.SENT :
			bMine = true;
			msg = getString(R.string.ps_sent_to_ps, fileName, nodeName);
			showToastMessage(msg);
			break;
		case IShareServiceListener.RECEIVED:

			bMine = false;
			msg = getString(R.string.ps_receive_from_ps, fileName, mMyNodeName);
			showToastMessage(msg);

			//Once received successfully merge the files
			new MergeTask(getApplicationContext()).execute();

			break;

		case IShareServiceListener.REJECTED:

			bMine = false;
			msg = getString(R.string.sending_ps_rejected_by_ps, fileName, nodeName);
			showToastMessage(msg);
			break;

		case IShareServiceListener.CANCELLED:
			bMine = nodeName.equals(mMyNodeName) ? true : false;
			if (!bMine) {
				AlertDialog alertDialog = mAlertDialogMap.get(exchangeId);
				if (alertDialog != null) {
					alertDialog.dismiss();
					mAlertDialogMap.remove(exchangeId);
				}
			}
			msg = getString(R.string.ps_transfer_cancelled_by_ps, fileName, nodeName);
			showToastMessage(msg);
			break;

		case IShareServiceListener.FAILED: 
			if (nodeName.equals(mMyNodeName)) {
				bMine = true;
				msg = getString(R.string.sending_ps_failed, fileName);
				showToastMessage(msg);
			} else {
				bMine = false;
				AlertDialog alertDialog = mAlertDialogMap.get(exchangeId);
				if (alertDialog != null) {
					alertDialog.dismiss();
					mAlertDialogMap.remove(exchangeId);
				}
				msg = getString(R.string.receiving_ps_failed, fileName);
				showToastMessage(msg);
			}
			break;

		default:
			showToastMessage("IShareServiceListener doesn't match");
			break;
		}
	}

	@Override
	public void onNodeEvent(String node, String channel, boolean bJoined) {

		// public Node joined
		if (bJoined) {
			if (channel.equals(mShareService.getPublicChannel())) {
				onPublicChannelNodeJoined(node);  
			} 

			return;
		}

		//public Node Leaved
		if (channel.equals(mShareService.getPublicChannel())) {
			onPublicChannelNodeLeaved(node); 
		} 



	}

	/**
	 * Send Connectivity status message to other nodes
	 * @param nodeName
	 * @param joined
	 */
	private void sendConnectivityStatusMessage(String nodeName, boolean joined) {
		boolean bSent = true;
		ArrayList<String> checkedList = mPublicChannelNodeListAdapter.getNodeList();

		final String connectionStatus = "Connection," + nodeName + "," + joined;

		for (String node : checkedList) {
			if (!mShareService.sendData(mShareService.getPublicChannel(), connectionStatus.getBytes(), node)) {
				bSent = false;
			}
		}

		if (!bSent) {
			appendLog("Connection Status : Message sending failed");
		}
	}

	@SuppressWarnings("unused")
	private boolean isPublicChannel(String channel) {
		if (channel.equals(mShareService.getPublicChannel())) {
			return true;
		}

		return false;
	}

	@Override
	public void onNetworkDisconnected() {
		mPublicChannelNodeListAdapter.clearAll();
		mMyIpAddressTextView.setText("Disconnected");
	}

	@Override
	public void onUpdateNodeInfo(String nodeName, String ipAddress) {
		setMyNodeInfo(nodeName, ipAddress);

	}

	@Override
	public void onConnectivityChanged() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.connect:

			sendMessage();

			//			//showToastMessage("TODO: Support to select the file in SDCard and send to node");
			//			ArrayList<String> checkedList = mPublicChannelNodeListAdapter.getNodeList();
			//			for (String nodeName : checkedList) {
			//				String exchangeId = mShareService.sendFile(mShareService.getPublicChannel(), tempFilePath + fileCache.getFileName(mTypeUrl.getText().toString()), nodeName);
			//				if (null == exchangeId || exchangeId.isEmpty()) {
			//					showToastMessage("Sending file failed");
			//				} else {
			//					onFileProgress(true, nodeName, mShareService.getPublicChannel(), 0, exchangeId);
			//				}
			//			}
			break;

		case R.id.wifidirect_button:

			startChord();
			break;

		case R.id.credits:
			//cc commons dialog box
			new AlertDialog.Builder(this)
			.setMessage("Background photo : steve_lodefink")
			.create()
			.show(); 
			break;
		}
	}

	public void startChord() {

		//Default starting the connect with WIFI
		//TODO: remove connection default to WIFI
		int nError = mShareService.start(Constants.INTERFACE_TYPE_WIFIP2P);

		if (ChordManager.ERROR_NONE == nError) {
			showToastMessage("Chord Started");
			mPublicChannelListView.setEnabled(true);
		} else if (ChordManager.ERROR_INVALID_INTERFACE == nError) {
			showToastMessage("Invalid connection");
		} else { //ERROR_INVALID_STATE, so what state should it be?
			//showToastMessage("Fail to start");
		}
	}

	public void stopChord() {
		mMyNodeNameTextView.setText("");
		mMyIpAddressTextView.setText("");
		mShareService.stop();
		mPublicChannelNodeListAdapter.clearAll();
	}

	private void showToastMessage(String message) {
		Toast.makeText(ShareActivity.this, message, Toast.LENGTH_SHORT).show();
	}

	public void setMyNodeInfo(String nodeName, String ipAddress) {
		mMyNodeName = nodeName;
		mMyNodeNameTextView.setText(nodeName);
		mMyIpAddressTextView.setText(ipAddress);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (parent == mPublicChannelListView) {
			String ipAddress = mShareService.getNodeIpAddress(mShareService.getPublicChannel(),
					mPublicChannelNodeListAdapter.getNodeName(position));
			Toast.makeText(this, ipAddress, Toast.LENGTH_SHORT).show();
			//sendMessage();
		} 
	}

	private void clearAllData() {
		mPublicChannelNodeListAdapter.clearAll();
	}

	public void onPublicChannelNodeJoined(String nodeName) {
		appendLog("[Public]Node join : " + nodeName);
		Log.d(TAG, "[Public]Node join : " + nodeName);
		mPublicChannelNodeListAdapter.addNode(nodeName);

		if (cd.isReallyConnected()) { //if joined send the connectivity status to other nodes.
			sendConnectivityStatusMessage(mMyNodeName, true);
		} else {
			sendConnectivityStatusMessage(mMyNodeName, false);
			appendLog("Connection Status : No Internet Connection (WIFI / MOBILE )");
		}

	}

	public void onPublicChannelNodeLeaved(String nodeName) {
		Log.d(TAG, "[Public]Node leave : " + nodeName);
		mPublicChannelNodeListAdapter.removeNode(nodeName);
	}

	/**
	 * Calculate file chunk size
	 * @param fileLength
	 * @param nodes
	 * @return
	 */
	private static ArrayList<String> calculateFileChunkSize(int fileLength, int nodes) {

		final int totalNodes = nodes + 1; // 1 for sender count
		final int numOfChunks = fileLength / totalNodes ; 

		ArrayList<String> chunkList = new ArrayList<String>();
		int x, y;

		for (int count=0; count< totalNodes ; count++) {
			x = numOfChunks * count ;
			y = x + numOfChunks - 1;

			if (count == (totalNodes -1)) {
				y += fileLength - y;
			}
			chunkList.add(x + "-" + y);
		}
		return chunkList;
	}

	/**
	 * Get Number of Nodes have connectivity to calculate the file chuck size and URL message will 
	 * be sent to nodes, which has Internet connection
	 * 
	 * SENDER to should know all the details,
	 * 
	 * How many nodes are having Internet Connectivity 
	 * 
	 */
	private ArrayList<String> getNumberOfNodesHaveConnectivity() {

		for (int node=0; node < mNodeConnection.size(); node++) {
			if (mNodeConnection.get(node).getConnected() == true) {
				mNodeContainsConnectivity.add(mNodeConnection.get(node).getNodeName());
			} else {
				// This will help me, to identify which nodes doesn't contain connectivity
				// later transfer the files to them. 
				mNodeNoConnectivity.add(mNodeConnection.get(node).getNodeName());
			}
		}
		return mNodeContainsConnectivity;
	}

	private void sendMessage() {

		// From now going to calculate file chunk size based on the nodes who are having internet connection
		ArrayList<String> nodeList = getNumberOfNodesHaveConnectivity();
		
		//Be default I am storing all the nodes in shared preferences for future references
		// Here I am not separating the nodes contain connectivity or not.
		//But we need some array list to know, now many nodes doesn't contain connectivity in proximity.
		ArrayList<String> checkedList = mPublicChannelNodeListAdapter.getNodeList();
		if (checkedList.size() != 0) {
			//Save number nodes to shared preferences
			dm.createNodeData(checkedList);
		}

		boolean bSent = true;

		// get Internet status
		isInternetPresent = cd.isReallyConnected();

		if (!isInternetPresent) { 
			appendLog("You don't have internet connection.");
		} else {
			final String message = Constants.URL_TO_DOWNLOAD;
			ArrayList<String> CHUNK_SIZE = null;
			byte[] originalByte = null;
			//find the file length
			try {
				originalByte = new CalculateFileLength(message).execute().get();
				final int totalNodes = nodeList.size();

				if (originalByte.length > 0) {
					CHUNK_SIZE = calculateFileChunkSize(originalByte.length, totalNodes);
				}

			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}

			//If number of nodes are zero, then sender should download the whole file bytes
			if (nodeList.size() != 0) {
				for (int i =0 ; i< nodeList.size(); i++) {
					final String msgDownload = message + "-" + CHUNK_SIZE.get(i + 1);
					if (!mShareService.sendData(mShareService.getPublicChannel(), msgDownload.getBytes(), nodeList.get(i))) {
						bSent = false;
					}
				}
			} else {
				//Download full bytes
				new DownloadAsync(getApplicationContext(), 
						Constants.URL_TO_DOWNLOAD, SEND_TYPE , 
						mPublicChannelNodeListAdapter, 0, originalByte.length).execute();
			}

			if(bSent) {
				//set the NodelistAdapter
				new NodeData().setNodeListAdapter(mPublicChannelNodeListAdapter);
				new DownloadAsync(getApplicationContext(), 
						Constants.URL_TO_DOWNLOAD, SEND_TYPE , 
						mPublicChannelNodeListAdapter,
						Integer.parseInt(splitString(CHUNK_SIZE.get(0))[0]),
						Integer.parseInt(splitString(CHUNK_SIZE.get(0))[1])
						).execute();
			}

			if (!bSent) {
				Toast.makeText(getApplicationContext(),
						getString(R.string.sending_failed_message), Toast.LENGTH_SHORT).show();
				return;
			}
		}

	}

	/**
	 * Split String
	 * @param inputString
	 * @return
	 */
	private String[] splitString(String inputString) {
		String delimiter = "-";
		String[] temp;
		temp = inputString.split(delimiter);
		return temp;
	}

	public void onFileNotify(String nodeName, String channel, String fileName, String transactionId) {
		displayFileNotify(nodeName, channel, fileName, transactionId);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mWifiDirectManager.registerReceiver();
		mNodeContainsConnectivity = new ArrayList<String>();
		mNodeNoConnectivity = new ArrayList<String>();

	}

	@Override
	protected void onPause() {
		//		if (mWifiDirectManager != null) {
		//			unregisterReceiver(mWifiDirectManager);
		//			mWifiDirectManager = null;
		//		}
		super.onPause();
	}

	/**
	 * Appends given message to the log console.
	 * 
	 * @param message
	 *            Message to log
	 */
	private void appendLog(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mLogConsole.setText(mLogConsole.getText() + "\n"
						+ DateFormat.getTimeInstance(DateFormat.MEDIUM).format(new Date()) + "\t\t" + message);
			}
		});
	}
}

