package com.netfaucet.share.utils;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 
 * Data Manager to store the slave nodes. 
 * To identify whether we have received the chunk files from all the nodes.
 *
 */
public class DataManager {

	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "netfaucet";

	// node
	public static final String KEY_NAME = "node";
	
	public static final String NODE_COUNT = "count";
	
	
	

	@SuppressLint("CommitPrefEdits")
	public DataManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void createNodeData(ArrayList<String> nodeList) {
		for(int i=0;i < nodeList.size();i++) {
			editor.putString(KEY_NAME + i,nodeList.get(i));
		}
		
		editor.putInt(NODE_COUNT, nodeList.size());
		// commit changes
		editor.commit();
	}
	
	
	/**
	 * Get the nodeList stored in Shared Preference
	 * @return nodeList
	 */
	public ArrayList<String> getNodeData() {
		
		ArrayList<String> nodeList =new ArrayList<String>();
		int size = pref.getInt(NODE_COUNT,0);

		for(int count=0; count<size; count++) {
			nodeList.add(pref.getString(KEY_NAME + count, null));
		}
		
		return nodeList;
		
	}
}
