package com.netfaucet.share.utils;

/**
 * 
 * Node Connection Information
 *
 */
public class NodeConnectionInfo {

	boolean isConnected = false;
	String nodeName = "";

	public NodeConnectionInfo(String message) {
		this.nodeName = message;

		if (message != null && message != "") {
			parseMessage(message);
		}
	}

	public void setConnected(boolean status) {
		this.isConnected  = status;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public boolean getConnected(){
		return isConnected;
	}

	public String getNodeName() {
		return nodeName;
	}

	private void parseMessage (String message) {
		String delimiter = ",";
		String[] temp;
		temp = message.split(delimiter);
		setConnected(Boolean.parseBoolean(temp[2]));
		setNodeName(temp[1]);
	}

}
