package com.netfaucet.share.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

/**
 * 
 * Calculate file length.
 * TODO: Need to refactor this class
 */
public class CalculateFileLength  extends AsyncTask<Void , Void, byte[]>  {

	private String url;
	
	public CalculateFileLength(String url) {
		this.url = url;
	}
	
	@Override
	protected byte[] doInBackground(Void... arg0) {
		return findFileLength(url);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	private byte[] findFileLength(String urlToDownload) {
		InputStream is  = null;

		byte[] splitData = null;
		try {
			URL urlDownload = new URL(urlToDownload);
			
			//open the connection again and connect
			HttpURLConnection connection = (HttpURLConnection)urlDownload.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoInput(true);
			connection.setDoOutput(true);


			connection.connect();

			if (connection.getResponseCode() /100 != 2) {
				//showToastMessage("HTTP request code not in range");
			}
			is = connection.getInputStream();
			
			splitData = readBytes(is);
			
		} catch (IOException e) {

		}

		return splitData;

	}

	
	/**
	 * Convert InputStream to byte array
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public byte[] readBytes(InputStream inputStream) throws IOException {
		// this dynamically extends to take the bytes you read
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		// this is storage overwritten on each iteration with bytes
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		// we need to know how may bytes were read to write them to the byteBuffer
		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}

		// and then we can return your byte array.
		return byteBuffer.toByteArray();
	}

	
	@Override
	protected void onPostExecute(byte[] result) {

	}

}
