package com.netfaucet.share.utils;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

/**
 * 
 * Internet Connection Dectector
 *
 */
public class ConnectionDetector {

	private Context _context;

	public ConnectionDetector(Context context){
		this._context = context;
	}

	public boolean isConnectingToInternet(){
		ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) 
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) 
				for (int i = 0; i < info.length; i++) 
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}

	/**
	 * Being connected to a network does not guarantee Internet connectivity.
	 * 
	 * Bottom line, network connectivity, does not mean Internet connectivity, it merely means that your device's 
	 * wireless hardware can connect to another host and both can make a connection.
	 * @return boolean
	 */
	public boolean isReallyConnected() {

		if (isConnectingToInternet()) {
			try {
				return new CheckInternetConnection().execute().get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}
		} else {
			return false; //no connectivity
		}
		return false;

	}

	/**
	 * To avoid MainThread exception created a new Async Task
	 *
	 */
	private class CheckInternetConnection extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				URL urlDownload = new URL(Constants.URL_TO_DOWNLOAD);

				// get content length and decide who will download first half and second half
				HttpURLConnection connection = (HttpURLConnection)urlDownload.openConnection();

				//open the connection again and connect
				connection = (HttpURLConnection)urlDownload.openConnection();
				connection.setConnectTimeout(3000); 
				connection.setRequestMethod("GET");
				connection.setDoInput(true);
				connection.setDoOutput(true);

				connection.connect();

				return (connection.getResponseCode() /100 != 2); 

			} catch (IOException e) {
				return false; //connectivity exists, but no Internet.
			}
		}

	}

}