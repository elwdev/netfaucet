package com.netfaucet.share.utils;

import com.samsung.chord.ChordManager;

public class Constants {
	
	public static final int INTERFACE_TYPE_WIFIP2P = ChordManager.INTERFACE_TYPE_WIFIP2P;
	public static final int INTERFACE_TYPE_WIFIAP = ChordManager.INTERFACE_TYPE_WIFIAP;
	public static final int INTERFACE_TYPE_WIFI = ChordManager.INTERFACE_TYPE_WIFI;
	public static final String URL_TO_DOWNLOAD = "http://www.engadget.com/rss.xml";
	
	public static final boolean INTERNAL_BUILD = true;
}
