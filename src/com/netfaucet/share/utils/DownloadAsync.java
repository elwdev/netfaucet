package com.netfaucet.share.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.netfaucet.share.adapter.NodeListAdapter;

/**
 * 
 * Download the files in new thread using AsyncTask API
 *  
 * TODO: need to check the performance while split and writing byte array to file system.
 * TODO: Need to clean this class
 */
public class DownloadAsync extends AsyncTask<Void , Void, String> {

	private Context context;
	private String url;
	private FileCache fileCache;
	private static final String TAG = "ShareActivity";
	private String type;
	private NodeListAdapter nodeListAdapter;
	private String status = "done";
	private int startValue = 0;
	private int endValue = 0;

	public DownloadAsync(Context context, String url, String type, NodeListAdapter nodeList, 
			int start, int end) {
		this.context = context;
		this.url = url;
		this.type = type;
		this.nodeListAdapter = nodeList;
		this.startValue = start;
		this.endValue = end;
		fileCache = new FileCache(context);

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(String result) {
		if(result.equals("done")) {
			showToastMessage("Successfully download!!!");
		} else {
			showToastMessage("Failed to download!!!");
		}
	}

	@Override
	protected String doInBackground(Void... arg0) {
		return downloadUrl(url);
	}


	@SuppressLint("NewApi")
	private String downloadUrl(String url) {
		FileOutputStream fos = null;
		InputStream is  = null;

		int lengthOfFile = 0;

		try {
			URL urlDownload = new URL(url);

			// get content length and decide who will download first half and second half
			HttpURLConnection connection = (HttpURLConnection)urlDownload.openConnection();
			connection.connect();
			lengthOfFile  = connection.getContentLength();
			Log.d(TAG, "Length of the content" + lengthOfFile);
			//I got the length, now i need to disconnect the connection
			connection.disconnect();

			//open the connection again and connect
			connection = (HttpURLConnection)urlDownload.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoInput(true);
			connection.setDoOutput(true);


			connection.connect();

			if (connection.getResponseCode() /100 != 2) {
				//showToastMessage("HTTP request code not in range");
			}

			fos = new FileOutputStream(fileCache.getFile(url));
			is = connection.getInputStream();

			final byte[] splitData = readBytes(is);
			byte[] totalLength = null; 
//			final int splitLength = splitData.length / ( getNumberOfNodes(nodeListAdapter).size() + 1);
			
			totalLength = Arrays.copyOfRange(splitData, startValue, endValue);
			
//			if (type.equals("send")) {
//				
//			} else {
//				/**
//				 * Assumption else part will be "receive"
//				 * Example : 50KB
//				 * 0----10----20----30----40----50
//				 *   S     R1    R2     R3   R4
//				 */  
//				//TODO: Need to do it. How going to split the data based on the receiver 
//				totalLength = Arrays.copyOfRange(splitData, splitLength, splitData.length);
//			} 


			if (totalLength.length > 0) {
				fos.write(totalLength);
			} else {
				showToastMessage("ByteArray less 0");
			}

		} catch (IOException e) {
			status = "fail";
			Log.d(TAG, "File Download Exception" + e.toString());
		} finally {
			if (fos != null) {
				try{
					fos.close();        
				} catch (Exception e) {
				}
			}

			if (is != null) {
				try{
					is.close();        
				} catch (Exception e) {
				}   
			}
		}
		return status;
	}

	

	private void showToastMessage(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
	
	
	/**
	 * Convert InputStream to byte array
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public byte[] readBytes(InputStream inputStream) throws IOException {
		// this dynamically extends to take the bytes you read
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		// this is storage overwritten on each iteration with bytes
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		// we need to know how may bytes were read to write them to the byteBuffer
		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}

		// and then we can return your byte array.
		return byteBuffer.toByteArray();
	}

}
