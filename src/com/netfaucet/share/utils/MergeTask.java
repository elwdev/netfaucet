package com.netfaucet.share.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

/**
 * Merge files
 * 
 * Merge "n" of files, which is under temp folder
 *
 */
public class MergeTask extends AsyncTask<Void, Void, String>{

	private Context context;
	private FileCache fileCache;
	private String status = "done";
	private String filePath = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/temp";
	DataManager dm;
	
	public MergeTask(Context context) {
		this.context = context;
		fileCache = new FileCache(context);
		dm = new DataManager(context);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}


	@Override
	protected void onPostExecute(String result) {
		if(result.equals("done")) {
			showToastMessage("Merged successfully");
		}
	}

	@Override
	protected String doInBackground(Void... arg0) {
		return mergeFiles();
	}

	/**
	 * Merge "n" of files, which is under temp folder
	 */
	public String mergeFiles() {

		ArrayList<String> nodeList = new ArrayList<String>();
		nodeList = dm.getNodeData();
		
		File file = new File(filePath);
		File list[] = file.listFiles();
		FileInputStream finStream = null;
		FileOutputStream fostream = null;
		int total=0;  
		File fileRead = null;

		Vector<FileInputStream> vector = new Vector<FileInputStream>();
		for( int i=0; i< list.length; i++) {
			try {
				
				fileRead = new File(list[i].toString());
				finStream = new FileInputStream(fileRead);
				vector.add(finStream);
				total += fileRead.length();
			} catch (FileNotFoundException e) {
			} 
		}

		Enumeration<FileInputStream> enumeration = vector.elements();

		SequenceInputStream bin = new SequenceInputStream(enumeration);
		int temp = 0;
		byte[] merge = new byte[total];  



		try {
			do {
				temp += bin.read(merge, temp, total - temp);  
			} while (temp != total); 

			fostream = new FileOutputStream(filePath+"/final" + fileCache.getFileExtension(Constants.URL_TO_DOWNLOAD));
			temp = 0;  
			fostream.write(merge, 0, merge.length);

		} catch (IOException e) {
		} finally {
			try {
				if (null != fostream) {
					fostream.close();
					fostream = null;
				}
			}catch (Exception e) {
			}

			try {
				if (null != finStream) {
					finStream.close();
					finStream = null;
				}
			}catch (Exception e) {
			}
		}
		return status;

	}
	


	private void showToastMessage(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}


}
