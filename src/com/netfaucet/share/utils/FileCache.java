package com.netfaucet.share.utils;

import java.io.File;

import android.content.Context;

public class FileCache {

	private File cacheDir;
	private File f;
	public FileCache(Context context) {

		if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),"temp");
		} else {
			cacheDir = context.getCacheDir();
		}

		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
	}

	public File getFile(String url){
		String filename = url.substring( url.lastIndexOf('/') + 1, url.length() );
		f = new File(cacheDir, filename);
		return f;
	}

	public void clear() {
		File[] files = cacheDir.listFiles();

		if(files == null) {
			return;
		}

		for (File f:files) {
			f.delete();
		}
	}

	public String getExternalStoragePath() {
		return f.getAbsolutePath();
	}

	public String getFileName(String url) {
		return url.substring( url.lastIndexOf('/')+1, url.length() );
	}

	/**
	 * Get the file extension
	 * @return file extension like xml, doc, ...
	 */
	public String getFileExtension(String url) {
		if ( url == null) {
			return "";
		}
		String[] urlSplit = url.split("/");
		String filename = urlSplit[urlSplit.length - 1];
		String[] nameSplit = filename.split("[.]");
		StringBuffer fileExtension = new StringBuffer();
		// to prevent appending . after extension type                                      
		if (nameSplit.length > 1) {
			for (int index = 1; index < nameSplit.length; index++) {
				if (index != nameSplit.length - 1)
					fileExtension.append(nameSplit[index] + ".");
				else
					fileExtension.append(nameSplit[index]);
			}
		} else {
			fileExtension.append(nameSplit[0]);
		}
		return "." + fileExtension.toString();
	}
}
