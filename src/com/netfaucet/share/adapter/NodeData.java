package com.netfaucet.share.adapter;

/**
 * 
 * Setter / Getter to get the Node Information
 * TODO: Can add more data to constructor
 *
 */

public class NodeData {

	private NodeListAdapter nodeListAdapter;


	public void setNodeListAdapter(NodeListAdapter nodeData) {
		nodeListAdapter = nodeData;
	}
	
	public NodeListAdapter getNodeListAdapter() {
		return nodeListAdapter;
	}


}
