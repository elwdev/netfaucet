package com.netfaucet.share.service;



import java.io.File;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.StatFs;
import android.util.Log;

import com.samsung.chord.ChordManager;
import com.samsung.chord.ChordManager.INetworkListener;
import com.samsung.chord.IChordChannel;
import com.samsung.chord.IChordChannelListener;
import com.samsung.chord.IChordManagerListener;

/**
 * 
 * ShareService
 * 
 * a) Start the chord manger
 * b) API Implementation to 
 *
 */
public class ShareService extends Service {
    private static final String TAG = "ShareService";

    private ChordManager mChord = null;

    private String mPrivateChannelName = "";

    private IShareServiceListener mListener = null;

    private PowerManager.WakeLock mWakeLock = null;
    private final IBinder mBinder = new ShareServiceBinder();

    private static final String SHARE_MESSAGE_TYPE = "SHARE_MESSAGE_TYPE";

    public static String SHARE_CHANNEL = "com.netfaucet.share.CHANNEL";

    private static final String MESSAGE_TYPE_FILE_NOTIFICATION = "FILE_NOTIFICATION_V2";

    private static final long SHARE_FILE_TIMEOUT_MILISECONDS = 1000 * 60 * 5;

    public static final String shareFilePath = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/temp";
    
    public static boolean ServiceStarted = false;

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        return mBinder;
    }


    public class ShareServiceBinder extends Binder {
        public ShareService getService() {
            return ShareService.this;
        }
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        super.onCreate();
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "onRebind()");
        super.onRebind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        return super.onStartCommand(intent, START_NOT_STICKY, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind()");
        return super.onUnbind(intent);
    }


    // Initialize chord
    public void initialize(IShareServiceListener listener) throws Exception {
        if (mChord != null) {
            return;
        }

        // #1. GetInstance
        mChord = ChordManager.getInstance(this);
        Log.d(TAG, "[Initialize] Chord Initialized");

        mListener = listener;

        // #2. set some values before start
        mChord.setTempDirectory(shareFilePath);
        mChord.setHandleEventLooper(getMainLooper());

        // Optional.
        // If you need listening network changed, you can set callback before
        // starting chord.
        mChord.setNetworkListener(new INetworkListener() {

            @Override
            public void onConnected(int interfaceType) {
                if (null != mListener) {
                    mListener.onConnectivityChanged();
                }
            }

            @Override
            public void onDisconnected(int interfaceType) {
                if (null != mListener) {
                    mListener.onConnectivityChanged();
                }
            }

        });
    }

    // Start chord
    public int start(int interfaceType) {

        acqureWakeLock();
        // #3. set some values before start
        return mChord.start(interfaceType, new IChordManagerListener() {
            @Override
            public void onStarted(String name, int reason) {
                Log.d(TAG, "onStarted chord");

                if (null != mListener)
                    mListener.onUpdateNodeInfo(name, mChord.getIp());

                if (STARTED_BY_RECONNECTION == reason) {
                    Log.e(TAG, "STARTED_BY_RECONNECTION");
                    return;
                }
                // if(STARTED_BY_USER == reason) : Returns result of calling
                // start

                // #4.(optional) listen for public channel
                IChordChannel channel = mChord.joinChannel(ChordManager.PUBLIC_CHANNEL,
                        mChannelListener);

                if (null == channel) {
                    Log.e(TAG, "fail to join public");
                }
            }

            @Override
            public void onNetworkDisconnected() {
                Log.e(TAG, "onNetworkDisconnected()");
                if (null != mListener)
                    mListener.onNetworkDisconnected();
            }

            @Override
            public void onError(int error) {
                // TODO Auto-generated method stub

            }

        });
    }


    private void acqureWakeLock(){
        if(null == mWakeLock){
            PowerManager powerMgr = (PowerManager)getSystemService(Context.POWER_SERVICE);
            mWakeLock = powerMgr.newWakeLock(PowerManager.FULL_WAKE_LOCK, "ChordApiDemo Lock");
            Log.d(TAG, "acqureWakeLock : new");
        }

        if(mWakeLock.isHeld()){
            Log.w(TAG, "acqureWakeLock : already acquire");
            mWakeLock.release();
        }

        Log.d(TAG, "acqureWakeLock : acquire");
        mWakeLock.acquire();
    }

    private void releaseWakeLock(){
        if(null != mWakeLock && mWakeLock.isHeld()){
            Log.d(TAG, "releaseWakeLock");
            mWakeLock.release();
        }
    }

    // This interface defines a listener for chord channel events.
    private IChordChannelListener mChannelListener = new IChordChannelListener() {

        /*
         * Called when a node join event is raised on the channel.
         * @param fromNode The node name corresponding to which the join event
         * has been raised.
         * @param fromChannel The channel on which the join event has been
         * raised.
         */
        @Override
        public void onNodeJoined(String fromNode, String fromChannel) {
            Log.v(TAG, "onNodeJoined(), fromNode : " + fromNode + ", fromChannel : "
                    + fromChannel);
            if (null != mListener)
                mListener.onNodeEvent(fromNode, fromChannel, true);
        }

        /*
         * Called when a node leave event is raised on the channel.
         * @param fromNode The node name corresponding to which the leave event
         * has been raised.
         * @param fromChannel The channel on which the leave event has been
         * raised.
         */
        @Override
        public void onNodeLeft(String fromNode, String fromChannel) {
            Log.v(TAG, "onNodeLeft(), fromNode : " + fromNode + ", fromChannel : "
                    + fromChannel);
            if (null != mListener)
                mListener.onNodeEvent(fromNode, fromChannel, false);
        }

        /*
         * Called when the data message received from the node.
         * @param fromNode The node name that the message is sent from.
         * @param fromChannel The channel name that is raised event.
         * @param payloadType User defined message type
         * @param payload Array of payload.
         */
        @Override
        public void onDataReceived(String fromNode, String fromChannel, String payloadType,
                byte[][] payload) {
            Log.v(TAG, "onDataReceived()");

            if (!SHARE_MESSAGE_TYPE.equals(payloadType))
                return;

            byte[] buf = payload[0];
            if (null != mListener)
                mListener.onReceiveMessage(fromNode, fromChannel, new String(buf));
        }

        /*
         * Called when the Share file notification is received. User can decide
         * to receive or reject the file.
         * @param fromNode The node name that the file transfer is requested by.
         * @param fromChannel The channel name that is raised event.
         * @param fileName File name
         * @param hash Hash value
         * @param fileType User defined file type
         * @param exchangeId Exchange ID
         * @param fileSize File size
         */
        @Override
        public void onFileWillReceive(String fromNode, String fromChannel, String fileName,
                String hash, String fileType, String exchangeId, long fileSize) {
            Log.d(TAG, "[originalName : " + fileName + " from : " + fromNode
                    + " exchangeId : " + exchangeId + " fileSize : " + fileSize + "]");

            File targetdir = new File(shareFilePath);
            if (!targetdir.exists()) {
                targetdir.mkdirs();
            }

            // Because the external storage may be unavailable,
            // you should verify that the volume is available before accessing it.
            // But also, onFileFailed with ERROR_FILE_SEND_FAILED will be called while Chord got failed to write file.
            StatFs stat = new StatFs(shareFilePath);              
            long blockSize = stat.getBlockSize();     
            long totalBlocks = stat.getAvailableBlocks();
            long availableMemory = blockSize * totalBlocks;

            if (availableMemory < fileSize) {
                rejectFile(fromChannel, exchangeId);
                if (null != mListener)
                    mListener.onFileCompleted(IShareServiceListener.FAILED, fromNode, fromChannel,
                            exchangeId, fileName);
                return;
            }

            if (null != mListener)
                mListener.onFileWillReceive(fromNode, fromChannel, fileName, exchangeId);
        }

        /*
         * Called when an individual chunk of the file is received. receive or
         * reject the file.
         * @param fromNode The node name that the file transfer is requested by.
         * @param fromChannel The channel name that is raised event.
         * @param fileName File name
         * @param hash Hash value
         * @param fileType User defined file type
         * @param exchangeId Exchange ID
         * @param fileSize File size
         * @param offset Chunk offset
         */
        @Override
        public void onFileChunkReceived(String fromNode, String fromChannel, String fileName,
                String hash, String fileType, String exchangeId, long fileSize, long offset) {
            if (null != mListener) {
                int progress = (int)(offset * 100 / fileSize);
                mListener.onFileProgress(false, fromNode, fromChannel, progress, exchangeId);
            }
        }

        /*
         * Called when the file transfer is completed from the node.
         * @param fromNode The node name that the file transfer is requested by.
         * @param fromChannel The channel name that is raised event.
         * @param fileName File name
         * @param hash Hash value
         * @param fileType User defined file type
         * @param exchangeId Exchange ID
         * @param fileSize File size
         * @param tmpFilePath Temporarily stored file path it is assigned with
         * setTempDirectory()
         */
        @Override
        public void onFileReceived(String fromNode, String fromChannel, String fileName,
                String hash, String fileType, String exchangeId, long fileSize, String tmpFilePath) {

        	 String savedName = fileName;

             int i = savedName.lastIndexOf(".");
             String name = savedName.substring(0, i);
             String ext = savedName.substring(i);

             File targetFile = new File(shareFilePath, savedName);
             
             while (targetFile.exists()) {
                 savedName = fromNode + ext;
                 targetFile = new File(shareFilePath, savedName);

                 Log.d(TAG, "onFileReceived : " + savedName);
             }

             File srcFile = new File(tmpFilePath);
             srcFile.renameTo(targetFile);

             if (null != mListener) {
                 mListener.onFileCompleted(IShareServiceListener.RECEIVED, fromNode, fromChannel,
                         exchangeId, savedName);
             }

        }
        

        /*
         * Called when an individual chunk of the file is sent.
         * @param toNode The node name to which the file is sent.
         * @param toChannel The channel name that is raised event.
         * @param fileName File name
         * @param hash Hash value
         * @param fileType User defined file type
         * @param exchangeId Exchange ID
         * @param fileSize File size
         * @param offset Offset
         * @param chunkSize Chunk size
         */
        @Override
        public void onFileChunkSent(String toNode, String toChannel, String fileName, String hash,
                String fileType, String exchangeId, long fileSize, long offset, long chunkSize) {

            if (null != mListener) {
                int progress = (int)(offset * 100 / fileSize);
                mListener.onFileProgress(true, toNode, toChannel, progress, exchangeId);
            }
        }

        /*
         * Called when the file transfer is completed to the node.
         * @param toNode The node name to which the file is sent.
         * @param toChannel The channel name that is raised event.
         * @param fileName File name
         * @param hash Hash value
         * @param fileType User defined file type
         * @param exchangeId Exchange ID
         */
        @Override
        public void onFileSent(String toNode, String toChannel, String fileName, String hash,
                String fileType, String exchangeId) {
            if (null != mListener) {
                mListener.onFileCompleted(IShareServiceListener.SENT, toNode, toChannel,
                        exchangeId, fileName);
            }
        }

        /*
         * Called when the error is occurred while the file transfer is in
         * progress.
         * @param node The node name that the error is occurred by.
         * @param channel The channel name that is raised event.
         * @param fileName File name
         * @param hash Hash value
         * @param exchangeId Exchange ID
         * @param reason The reason for failure could be one of
         * #ERROR_FILE_CANCELED #ERROR_FILE_CREATE_FAILED
         * #ERROR_FILE_NO_RESOURCE #ERROR_FILE_REJECTED #ERROR_FILE_SEND_FAILED
         * #ERROR_FILE_TIMEOUT
         */
        @Override
        public void onFileFailed(String node, String channel, String fileName, String hash,
                String exchangeId, int reason) {
            switch (reason) {
            case ERROR_FILE_REJECTED: {
                Log.e(TAG, "onFileFailed() - REJECTED Node : " + node
                        + ", fromChannel : " + channel);
                if (null != mListener) {
                    mListener.onFileCompleted(IShareServiceListener.REJECTED, node, channel,
                            exchangeId, fileName);
                }
                break;
            }

            case ERROR_FILE_CANCELED: {
                Log.e(TAG, "onFileFailed() - CANCELED Node : " + node
                        + ", fromChannel : " + channel);
                if (null != mListener) {
                    mListener.onFileCompleted(IShareServiceListener.CANCELLED, node, channel,
                            exchangeId, fileName);
                }
                break;
            }
            case ERROR_FILE_CREATE_FAILED:
            case ERROR_FILE_NO_RESOURCE:
            default:
                Log.e(TAG, "onFileFailed() - " + reason + " : " + node
                        + ", fromChannel : " + channel);
                if (null != mListener) {
                    mListener.onFileCompleted(IShareServiceListener.FAILED, node, channel,
                            exchangeId, fileName);
                }
                break;
            }

        }
    };

    // Reject to receive file.
    public boolean rejectFile(String fromChannel, String coreTransactionId) {
        Log.d(TAG, "rejectFile()");
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(fromChannel);
        if (null == channel) {
            Log.e(TAG, "cancelFile() : invalid channel instance");
            return false;
        }

        // @param exchangeId Exchanged ID
        return channel.rejectFile(coreTransactionId);
    }

    // Requests for nodes on the channel.
    public List<String> getJoinedNodeList(String channelName) {
        Log.d(TAG, "getJoinedNodeList()");
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(channelName);
        if (null == channel) {
            Log.e(TAG, "getJoinedNodeList() : invalid channel instance-" + channelName);
            return null;
        }

        return channel.getJoinedNodeList();
    }

    // Send data message to the node.
    public boolean sendData(String toChannel, byte[] buf, String nodeName) {
        if (mChord == null) {
            Log.v(TAG, "sendData : mChord IS NULL  !!");
            return false;
        }
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(toChannel);
        if (null == channel) {
            Log.e(TAG, "sendData : invalid channel instance");
            return false;
        }

        if (nodeName == null) {
            Log.v(TAG, "sendData : NODE Name IS NULL !!");
            return false;
        }

        byte[][] payload = new byte[1][];
        payload[0] = buf;

        Log.v(TAG, "sendData : " + new String(buf) + ", des : " + nodeName);

        /*
         * @param toNode The joined node name that the message is sent to. It is
         * mandatory.
         * @param payloadType User defined message type. It is mandatory.
         * @param payload The package of data to send
         * @return Returns true when file transfer is success. Otherwise, false
         * is returned
         */
        if (false == channel.sendData(nodeName, SHARE_MESSAGE_TYPE, payload)) {
            Log.e(TAG, "sendData : fail to sendData");
            return false;
        }

        return true;
    }


    // Send file to the node on the channel.
    public String sendFile(String toChannel, String strFilePath, String toNode) {
        Log.d(TAG, "sendFile() ");

        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(toChannel);
        if (null == channel) {
            Log.e(TAG, "sendFile() : invalid channel instance");
            return null;
        }
        /*
         * @param toNode The node name that the file is sent to. It is
         * mandatory.
         * @param fileType User defined file type. It is mandatory.
         * @param filePath The absolute path of the file to be transferred. It
         * is mandatory.
         * @param timeoutMsec The time to allow the receiver to accept the
         * receiving data requests.
         */
        return channel.sendFile(toNode, MESSAGE_TYPE_FILE_NOTIFICATION, strFilePath,
                SHARE_FILE_TIMEOUT_MILISECONDS);
    }

    // Accept to receive file.
    public boolean acceptFile(String fromChannel, String exchangeId) {
        Log.d(TAG, "acceptFile()");
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(fromChannel);
        if (null == channel) {
            Log.e(TAG, "acceptFile() : invalid channel instance");
            return false;
        }

        /*
         * @param exchangeId Exchanged ID
         * @param chunkTimeoutMsec The timeout to request the chunk data.
         * @param chunkRetries The count that allow to retry to request chunk
         * data.
         * @param chunkSize Chunk size
         */
        return channel.acceptFile(exchangeId, 30*1000, 2, 300 * 1024);
    }

    // Cancel file transfer while it is in progress.
    public boolean cancelFile(String channelName, String exchangeId) {
        Log.d(TAG, "cancelFile()");
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(channelName);
        if (null == channel) {
            Log.e(TAG, "cancelFile() : invalid channel instance");
            return false;
        }

        // @param exchangeId Exchanged ID
        return channel.cancelFile(exchangeId);
    }



    // Send data message to the all nodes on the channel.
    public boolean sendDataToAll(String toChannel, byte[] buf) {
        if (mChord == null) {
            Log.v(TAG, "sendDataToAll : mChord IS NULL  !!");
            return false;
        }

        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(toChannel);
        if (null == channel) {
            Log.e(TAG, "sendDataToAll : invalid channel instance");
            return false;
        }

        byte[][] payload = new byte[1][];
        payload[0] = buf;

        Log.v(TAG, "sendDataToAll : " + new String(buf));

        /*
         * @param payloadType User defined message type. It is mandatory.
         * @param payload The package of data to send
         * @return Returns true when file transfer is success. Otherwise, false
         * is returned.
         */
        if (false == channel.sendDataToAll(SHARE_MESSAGE_TYPE, payload)) {
            Log.e(TAG, "sendDataToAll : fail to sendDataToAll");
            return false;
        }

        return true;
    }

    /*
     * Set a keep-alive timeout. Node has keep-alive timeout. The timeoutMsec
     * determines the maximum keep-alive time to wait to leave when there is no
     * data from the nodes. Default time is 15000 millisecond.
     */
    public void setNodeKeepAliveTimeout(long timeoutMsec) {
        Log.d(TAG, "setNodeKeepAliveTimeout()");
        // @param timeoutMsec Timeout with millisecond.
        mChord.setNodeKeepAliveTimeout(timeoutMsec);
    }

    // Get an IPv4 address that the node has.
    public String getNodeIpAddress(String channelName, String nodeName) {
        Log.d(TAG, "getNodeIpAddress() channelName : " + channelName + ", nodeName : "
                + nodeName);
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChord.getJoinedChannel(channelName);
        if(null == channel){
            Log.e(TAG, "getNodeIpAddress : invalid channel instance");
            return "";
        }

        /*
         * @param nodeName The node name to find IPv4 address.
         * @return Returns an IPv4 Address.When there is not the node name in
         * the channel, null is returned.
         */
        return channel.getNodeIpAddress(nodeName);
    }

    // Get a list of available network interface types.
    public List<Integer> getAvailableInterfaceTypes() {
        Log.d(TAG, "getAvailableInterfaceTypes()");
        /*
         * @return Returns a list of available network interface types.
         * #INTERFACE_TYPE_WIFI Wi-Fi #INTERFACE_TYPE_WIFIAP Wi-Fi mobile
         * hotspot #INTERFACE_TYPE_WIFIP2P Wi-Fi Direct
         */
        return mChord.getAvailableInterfaceTypes();
    }

    // Request for joined channel interfaces.
    public List<IChordChannel> getJoinedChannelList() {
        Log.d(TAG, "getJoinedChannelList()");
        // @return Returns a list of handle for joined channel. It returns an
        // empty list, there is no joined channel.
        return mChord.getJoinedChannelList();
    }

    // Join a desired channel with a given listener.
    public IChordChannel joinChannel(String channelName) {
        Log.d(TAG, "joinChannel()" + channelName);
        if (channelName == null || channelName.equals("")) {
            Log.e(TAG, "joinChannel > " + mPrivateChannelName
                    + " is invalid! Default private channel join");
            mPrivateChannelName = SHARE_CHANNEL;
        } else {
            mPrivateChannelName = channelName;
        }

        /*
         * @param channelName Channel name. It is a mandatory input.
         * @param listener A listener that gets notified when there is events in
         * joined channel mandatory. It is a mandatory input.
         * @return Returns a handle of the channel if it is joined successfully,
         * null otherwise.
         */
        IChordChannel channelInst = mChord.joinChannel(mPrivateChannelName, mChannelListener);

        if (null == channelInst) {
            Log.d(TAG, "fail to joinChannel! ");
            return null;
        }

        return channelInst;
    }

    // Leave a given channel.
    public void leaveChannel() {
        Log.d(TAG, "leaveChannel()");
        // @param channelName Channel name
        mChord.leaveChannel(mPrivateChannelName);
        mPrivateChannelName = "";
    }

    // Stop chord
    public void stop() {
        Log.d(TAG, "stop()");
        releaseWakeLock();
        if (mChord != null) {
            mChord.stop();
        }
    }

    public String getPublicChannel() {
        return ChordManager.PUBLIC_CHANNEL;
    }

    public String getPrivateChannel() {
        return mPrivateChannelName;
    }


}
