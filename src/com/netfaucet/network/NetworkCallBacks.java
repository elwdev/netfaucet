package com.netfaucet.network;

/**
 * 
 * Callbacks for network connectivity
 *
 */
public interface NetworkCallBacks {

	/**
	 * Called when connectivity is successfully
	 */
	void connected();
	
	/**
	 * Called when connectivity is failure
	 */
	void disconnected();
	
	/**
	 * Called when connection type is know 
	 */
	void connectionType(int type);
}
