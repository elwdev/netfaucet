package com.netfaucet.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 
 * NetworkChangeReceiver
 *
 */
public class NetworkChangeReceiver extends BroadcastReceiver{

	private final Context mContext;
	private ConnectivityManager cm;

	private static int TYPE_WIFI = 1;
	private static int TYPE_MOBILE = 2;
	private static int TYPE_NOT_CONNECTED = 0;
	private final NetworkCallBacks mCallbacks;

	public NetworkChangeReceiver(final Context context, final NetworkCallBacks callbacks) {
		mContext = context;
		mCallbacks = callbacks;
		cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	/**
	 * Registers for receiving broadcasts.
	 */
	public void registerReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		mContext.registerReceiver(this, filter);
	}

	/**
	 * Unregisters for receiving broadcasts
	 */
	public void unregisterReceiver() {
		mContext.unregisterReceiver(this);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (!intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
			return;
		mCallbacks.connectionType(getConnectivityStatus(context));
	}

	/**
	 * Get connectivity status
	 * @param context
	 * @return connection type
	 */
	public int getConnectivityStatus(Context context) {
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				return TYPE_WIFI;

			if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
				return TYPE_MOBILE;
		} 
		return TYPE_NOT_CONNECTED;
	}
}
