package com.netfaucet.wifip2p;

import java.net.InetAddress;

/**
 * Callbacks for WifiDirectManager.
 */
public interface WifiDirectCallbacks {
    /**
     * Called when connection cannot be established.
     */
    void connectFailed();

    /**
     * Called when connection is successfully established.
     * 
     * @param address
     *            Group owner address
     * @param isMyAddress
     *            If current device is group owner
     */
    void connected(InetAddress address, boolean isMine);

    /**
     * Called when WiFi Direct is not supported or disabled.
     */
    void unavailable();

    /**
     * Called when discover is successfully finished - peer with given MAC was found.
     */
    void discoverFinished(String deviceAddress);

    /**
     * Called when current device MAC is known.
     */
    void gotMyMac();
    
    void initializeChord();
}